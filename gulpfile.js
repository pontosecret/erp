"use strict";

const { watch, series, parallel, src, dest } = require('gulp');
const favicons   = require('gulp-favicons');
const babel      = require('gulp-babel');
const uglify     = require('gulp-uglify');
const sass       = require('gulp-sass')(require('sass'));
const rename     = require('gulp-rename');
const prefixer   = require('gulp-autoprefixer');
const clean      = require('gulp-clean');
const concat     = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const cleanCSS   = require('gulp-clean-css');
const image      = require('gulp-image');

function cleanup(cb) {
    return src(['public/static/css','public/static/fonts','public/static/js','public/static/media'], {read: false, 'allowEmpty': true})
        .pipe(clean());
}

function cssMinify(cb) {
    src(['assets/scss/*.scss'])
        .pipe(sass())
        .pipe(prefixer())
        .pipe(sourcemaps.init())
        .pipe(cleanCSS())
        .pipe(rename({ extname: '.min.css' }))
        .pipe(sourcemaps.write())
        .pipe(dest('public/static/css'))
    ;

    cb();
}

function cssLib(cb) {
    cb();
}

function jsLib(cb) {
    return src([
        'node_modules/moment/min/moment-with-locales.js',
    ])
        .pipe(concat('lib.js'))
        .pipe(rename({ extname: '.min.js' }))
        .pipe(dest('public/static/js'))
    ;
}

function jsApp(cb) {
    return src('assets/js/**/*.js')
        .pipe(babel())
        .pipe(uglify())
        .pipe(rename({ extname: '.min.js' }))
        .pipe(dest('public/static/js'))
    ;
}

function media(cb) {
    cb();

    return src(['./assets/media/**/*'])
        .pipe(image())
        .pipe(dest('./public/static/media'))
    ;
}

function fonts(cb) {
    cb();

//    return src(['./assets/fonts/**/*'])
//        .pipe(image())
//        .pipe(dest('./public/static/fonts'))
//    ;
}

function favicon(cb) {
    cb();

//     return src('./assets/media/favicons/42logo.png')
//         .pipe(
//             favicons({
//                 appName: '42 - ERP',
//                 appShortName: 'ERP',
//                 appDescription: 'Timetracker',
//                 developerName: 'Florian Hervieu',
//                 developerURL: 'https://www.42stores.com/',
//                 background: '#020307',
//                 path: '/static/favicons/',
//                 url: 'https://www.42stores.com/',
//                 display: 'standalone',
//                 orientation: 'portrait',
//                 scope: '/',
//                 start_url: '',
//                 version: 1.0,
//                 logging: false,
//                 html: 'index.html',
//                 pipeHTML: true,
//                 replace: true,
//             })
//         )
//         .pipe(dest('./public/static/favicons'));
}

function publish(cb) {
    // body omitted
    cb();
}

function watchFiles(cb) {
    watch('assets/js/**/*.js', series(jsApp));
    watch('assets/scss/**/*.scss', series(cssMinify));
    watch('assets/media/**/*', series(media));
    watch('assets/fonts/**/*', series(fonts));

    cb();
}

exports.default = series(
    cleanup,
    parallel(jsApp, jsLib),
    parallel(cssMinify, cssLib),
    media,
    fonts,
    favicon,
    publish,
    watchFiles
);

exports.deploy = series(
    cleanup,
    parallel(jsApp, jsLib),
    parallel(cssMinify, cssLib),
    media,
    favicon,
    fonts,
    publish
);
