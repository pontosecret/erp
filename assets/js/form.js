(function () {
    document.querySelectorAll('.add-another-collection-widget').addEventListener('click', function (e) {
        var list = document.querySelector('#' + e.target.attributes['data-list-selector'].value);
        var counter = list.attributes['data-widget-counter'] ? list.attributes['data-widget-counter'].value : list.childNodes.length;
        var newWidget = list.attributes['data-prototype'].value;

        newWidget = newWidget.replace(/__name__/g, counter);

        counter++;

        list.setAttribute('widget-counter', counter);

        var wrapper = document.createElement('div');
        wrapper.innerHTML= newWidget;
        var div = wrapper.firstChild;
        list.append(div);
    });
})();