"use strict";

/**
 * EventTarget
 *
 * @param selector
 * @returns {*}
 */
EventTarget.prototype.parent = function(selector) {
    let elm = this;
    while ((elm = elm.parentElement) !== null) {
        if (elm.matches(selector)) {
            return elm;
        }
    }

    return null;
};

/**
 * NodeList
 *
 * @param event
 * @param callback
 */
NodeList.prototype.addEventListener = function (event, callback) {
    this.forEach(function(elm) {
        elm.addEventListener(event, callback);
    });

    return this;
};

/**
 * NodeList
 *
 * @param event
 * @param selector
 * @param callback
 */
NodeList.prototype.addEventListenerFor = function (event, selector, callback) {
    this.forEachOf(selector, function(elm) {
        elm.addEventListener(event, callback);
    });

    return this;
};

/**
 * NodeList
 *
 * @param selector
 * @param callback
 */
NodeList.prototype.forEachOf = function(selector, callback) {
    this.forEach(function(elm) {
        if (elm.matches(selector)) {
            callback(elm);
        }
    });

    return this;
};

/**
 * Node
 *
 * @param event
 * @param selector
 * @param callback
 */
Node.prototype.addEventListenerFor = function (event, selector, callback) {
    if (this.matches(selector)) {
        this.addEventListener(event, callback);
    }

    return this;
};

Node.prototype.empty = function() {
    console.log(this.length);
    this.childNodes.forEach(function(child) {
        child.remove();
    });
    console.log(this.length);

    return this;
};

let app = {};

app.utils = {
    formatBytes: function(bytes, decimals = 2) {
        if (bytes === 0) return '0 o';

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['o', 'Ko', 'Mo', 'Go', 'To', 'Po', 'Eo', 'Zo', 'Yo'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    },

    query: function(method, url, data) {
        console.log(method, url, data);
        return new Promise(function(resolve, reject) {
            if (method == 'GET') {
                const urlParams = new URLSearchParams(data).toString();
                
                if (urlParams) {
                    url = url + '?' + urlParams;
                }
            }

            let xhr = new XMLHttpRequest();
            xhr.open(method, url);

            xhr.send(data);

            xhr.onload = function (e) {
                if (this.readyState === 4) {
                    if (this.status >= 200 && this.status < 300) {
                        let event = new CustomEvent('Net.Success');
                        event.xhr = this;

                        document.dispatchEvent(event);

                        return resolve(this.responseText);
                    } else {
                        let event = new CustomEvent('Net.Error');
                        event.xhr = this;

                        document.dispatchEvent(event);

                        reject(this.statusText);
                    }
                }
            };
        });
    },

    numberFormat: function(number, decimals, decPoint, thousandsSep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        let n = !isFinite(+number) ? 0 : +number;
        let prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
        let sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep;
        let dec = (typeof decPoint === 'undefined') ? '.' : decPoint;
        let s;

        let toFixedFix = function (n, prec) {
            if (('' + n).indexOf('e') === -1) {
                return +(Math.round(n + 'e+' + prec) + 'e-' + prec)
            } else {
                var arr = ('' + n).split('e');
                var sig = '';
                if (+arr[1] + prec > 0) {
                    sig = '+'
                }
                return (+(Math.round(+arr[0] + 'e' + sig + (+arr[1] + prec)) + 'e-' + prec)).toFixed(prec)
            }
        };

        s = (prec ? toFixedFix(n, prec).toString() : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }

        return s.join(dec);
    }
};

app.Date = class {
    #date;
    #formatConfig;


    constructor(date) {
        if (date !== undefined) {
            if (typeof date === 'object') {
                this.date = date;
            } else {
                this.date = new Date(str);
            }
        } else {
            this.date = new Date();
        }
    }

    /**
     * @param str
     * @returns String
     */
    format(str) {
        let formattedDate = str;
        if (this.formatConfig === undefined) {
            this.formatConfig = {
                'd': this.getDate({day: '2-digit'}),
                'D': this.getWeekDay({weekday: 'short'}),
                'j': this.getDate({day: 'numeric'}),
                'l': this.getWeekDay({weekday: 'long'}),
                'N': this.getWeekDay({weekday: 'index'}),
                'S': '', //-- st, nd, rd, th
                'w': this.getWeekDay({weekday: 'iso'}),
                'z': '', //-- Day number (in year)
                'W': '', //-- Week number
                'F': this.getMonth({month: 'long'}),
                'm': this.getMonth({month: '2-digit'}),
                'M': this.getMonth({month: 'short'}),
                'n': this.getMonth({month: 'numeric'}),
                't': '', //-- Number of days in month
                'L': '', //-- Is year bisextile
                'o': '', //-- Year (ISO)
                'Y': this.getYear({year: 'numeric'}),
                'y': this.getYear({year: '2-digit'}),
                'a': (this.getHour() < 12 ? 'am' : 'pm'),
                'A': (this.getHour() < 12 ? 'AM' : 'PM'),
                'B': this.getSwatchTime(), //-- Swatch internet hour
                'g': this.getHour({hour: 'numeric', hour12: true}),
                'G': this.getHour({hour: 'numeric', hour12: false}),
                'h': this.getHour({hour: '2-digit', hour12: true}),
                'H': this.getHour({hour: '2-digit', hour12: false}),
                'i': this.getMinute({minute: '2-digit'}),
                's': this.getSecond({second: '2-digit'}),
                'u': '', //-- Microseconds
                'v': this.getMilliseconds(), //-- Milliseconds
                'e': this.getTimezone({timezone: 'long'}),
                'I': this.isDSTObserved(), //-- Daylight saving time
                'O': this.getTimezoneOffset({timezoneOffset: 'hours'}), //-- Difference with GMT (ex: +0200)
                'P': this.getTimezoneOffset({timezoneOffset: 'hours', separator: ':'}), //-- Difference with GMT (ex: +02:00)
                'T': this.getTimezone({timezone: 'short'}),
                'Z': this.getTimezoneOffset({timezoneOffset: 'seconds'}), //-- Timezone in seconds
                'c': '', //-- ISO 8601 date format
                'r': '', //-- RFC 2822 date format
                'U': '', //-- UNIX seconds
            };
        }

        let regex = /[a-z]/gi;
        let match;
        let reversedMatches = [];

        while ((match = regex.exec(str)) !== null) {
            if (str[match.index - 1] === '%') {
                continue;
            }
            reversedMatches.unshift(match);
        }

        reversedMatches.forEach((match) => {
            formattedDate = formattedDate.substr(0, match.index) + this.formatConfig[match[0]] + formattedDate.substr(match.index + 1);
        });

        return formattedDate;
    }

    getYear(option) {
        let year;

        option      = option || {};
        option.year = option.year || null;

        switch (option.year) {
            case '2-digit':
                year = this.date.getFullYear().toString().substr(-2);
                break;

            case 'numeric':
            default:
                year = this.date.getFullYear();
                break;
        }

        return year;
    }

    getMonth(option) {
        let month;

        option       = option || {};
        option.month = option.month || null;

        switch (option.month) {
            case 'long':
                month = new Intl.DateTimeFormat(option.locale || undefined, {month: 'long'}).format(this.date);
                break;

            case 'short':
                month = new Intl.DateTimeFormat(option.locale || undefined, {month: 'short'}).format(this.date);
                break;

            case '2-digit':
                month = this.date.getMonth().toString().padStart(2, '0');
                break;

            case 'numeric':
            default:
                month = this.date.getUTCMonth();
                break;
        }

        return month;
    }

    getDate(option) {
        let date;

        option      = option || {};
        option.date = option.date || null;

        switch (option.date) {
            case '2-digit':
                date = this.date.getDate().toString().padStart(2, '0');
                break;

            case 'numeric':
            default:
                date = this.date.getDate();
                break;
        }

        return date;
    }

    getHour(option) {
        let hour;

        option      = option || {};
        option.hour = option.hour || null;

        switch (option.hour) {
            case '2-digit':
                hour = this.date.getHours();
                if (option.hour12 !== undefined && option.hour12 === true && hour > 12) {
                    hour -= 12;
                }
                hour = hour.toString().padStart(2, '0');
                break;

            case 'numeric':
            default:
                hour = this.date.getHours();
                if (option.hour12 !== undefined && option.hour12 === true && hour > 12) {
                    hour -= 12;
                }
                break;
        }

        return hour;
    }

    getMinute(option) {
        let minute;

        option        = option || {};
        option.minute = option.minute || null;

        minute = this.date.getMinutes();

        if (option.minute === '2-digit') {
            minute = minute.toString().padStart(2, '0');
        }

        return minute;
    }

    getSecond(option) {
        let second;

        option        = option || {};
        option.second = option.second || null;

        second = this.date.getSeconds();

        if (option.second === '2-digit') {
            second = second.toString().padStart(2, '0');
        }

        return second;
    }

    getMilliseconds() {
        return this.date.getMilliseconds();
    }

    getWeekDay(option) {
        let weekDay;

        switch (option.weekday) {
            case "index":
            case "iso":
                weekDay = this.date.getDay();
                if (option.weekday === 'iso') {
                    weekDay -= 1;
                }
                break;

            case 'short':
                weekDay = new Intl.DateTimeFormat(option.locale || undefined, {weekday: 'short'}).format(this.date);
                break;

            case "long":
                weekDay = new Intl.DateTimeFormat(option.locale || undefined, {weekday: 'long'}).format(this.date);
                break;
        }

        return weekDay;
    }

    getTimezone(option) {
        let timezone;

        option          = option || {};
        option.timezone = option.timezone || null;

        switch (option.timezone) {
            case 'short':
                timezone = new Intl.DateTimeFormat(option.locale || undefined, {timeZoneName: 'short'}).format(this.date);
                break;

            case "long":
                timezone = new Intl.DateTimeFormat(option.locale || undefined, {timeZoneName: 'long'}).format(this.date);
                break;
        }

        return timezone;
    }

    getTimezoneOffset(option) {
        let timezoneOffset;

        option                = option || {};

        timezoneOffset = -this.date.getTimezoneOffset();
        switch (option.timezoneOffset) {
            case 'seconds':
                timezoneOffset *= 60;
                break;

            case 'hours':
                timezoneOffset /= 60;
                timezoneOffset = (timezoneOffset >= 0 ? '+' : '') + timezoneOffset.toString().padStart(2, '0') + (option.separator || '') + '00';
                break;
        }

        return timezoneOffset;
    }

    isDSTObserved() {
        let jan = new Date(this.date.getFullYear(), 0, 1).getTimezoneOffset();
        let jul = new Date(this.date.getFullYear(), 6, 1).getTimezoneOffset();

        return Math.max(jan, jul) !== this.date.getTimezoneOffset();
    }

    getSwatchTime() {
        return Math.floor((360*this.date.getHours()+60*this.date.getMinutes()+this.date.getSeconds())/86.4)
    }
};
