<?php

namespace App\Form;

use App\Entity\EscapeGame;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EscapeGameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $timeSlotsOptions = [
            'entry_type' => CollectionType::class,
            'entry_options' => [
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
            ],
            'allow_add' => false,
            'allow_delete' => false,
            'prototype' => false,
            'delete_empty' => false,
            'attr' => [
                'class' => 'column'
            ],
            'empty_data' => [
                [''],[''],[''],[''],[''],[''],['']
            ]
        ];

        if (!$options['data']->id) {
            $timeSlotsOptions['data'] = [[''],[''],[''],[''],[''],[''],['']];
        }

        $builder
            ->add('name', TextType::class)
            ->add('description', TextareaType::class)
            ->add('minSize', IntegerType::class)
            ->add('maxSize', IntegerType::class)
            ->add('timeSlots', CollectionType::class, $timeSlotsOptions)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EscapeGame::class,
        ]);
    }
}