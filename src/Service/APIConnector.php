<?php

namespace App\Service;

use App\Exception\ApiException;
use App\Exception\BadRequestApiException;
use App\Exception\UnauthorizedApiException;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class APIConnector
{
    private TokenStorageInterface $tokenStorage;
    private string                $apiUrl;

    public function __construct(string $apiUrl, TokenStorageInterface $tokenStorage)
    {
        $this->apiUrl       = $apiUrl;
        $this->tokenStorage = $tokenStorage;
    }

    public function query($method, $path, $criteria=[]): string
    {
        $headers     = [
            'Accept'       => 'application/ld+json',
            'Content-Type' => 'application/json'
        ];
        if ($this->tokenStorage->getToken()) {
            $headers['Authorization'] = "Bearer ".$this->tokenStorage->getToken()->getUser()->jwt;
        }
        
        $client = new Client();
        try {
            switch ($method) {
                case 'GET':
                    $response = $client->get($this->apiUrl.$path.($criteria ? '?'.http_build_query($criteria) : null), [
                        'headers' => $headers,
                    ]);
                    break;
    
                case 'POST':
                    $response = $client->post($this->apiUrl.$path, [
                        'headers' => $headers,
                        'body'    => json_encode($criteria)
                    ]);
                    break;
    
                case 'PUT':
                    $response = $client->put($this->apiUrl.$path, [
                        'headers' => $headers,
                        'body'    => json_encode($criteria)
                    ]);
                    break;
            }
        } catch (ClientException|ServerException $e) {
            $this->handleException($e);
            dd($e);
        }

        $responseBody = '';
        while ($chunk = $response->getBody()->read(1024)) {
            $responseBody .= $chunk;
        }

        return $responseBody;
    }

    public function authenticate($username, $password) 
    {
        return json_decode(
            $this->query('POST', '/authentication_token', ['email' => $username, 'password' => $password]),
            true
        );
    }

    private function handleException(ClientException|ServerException $exception)
    {
        if ($exception->hasResponse()) {
            $responseBody = '';
            while ($chunk = $exception->getResponse()->getBody()->read(1024)) {
                $responseBody .= $chunk;
            }

            dd(json_decode($responseBody));

            switch ($exception->getResponse()->getStatusCode()) {
                case Response::HTTP_BAD_REQUEST:
                    throw new BadRequestApiException($responseBody, Response::HTTP_BAD_REQUEST);
                    
                case Response::HTTP_UNAUTHORIZED:
                    throw new UnauthorizedApiException();

                case Response::HTTP_NOT_FOUND:
                    throw new NotFoundHttpException($exception->getRequest()->getUri());

                case Response::HTTP_INTERNAL_SERVER_ERROR:
                    throw new ApiException($responseBody, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }
}