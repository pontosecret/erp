<?php

namespace App\Entity;

use App\Repository\CustomerRepository;

class Customer extends AbstractEntity
{
    public static function getRepositoryClass(): string
    {
        return CustomerRepository::class;
    }
}