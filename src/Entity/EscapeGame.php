<?php

namespace App\Entity;

use App\Repository\EscapeGameRepository;

class EscapeGame extends AbstractEntity
{
    public ?int $id = null;
    public string $name;
    public string $description;
    public array $timeSlots;
    public int $minSize;
    public int $maxSize;

    public static function getRepositoryClass(): string
    {
        return EscapeGameRepository::class;
    }
}