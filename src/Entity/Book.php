<?php

namespace App\Entity;

use App\Repository\BookRepository;

class Book extends AbstractEntity
{
    public $id;
    public $isbn;
    public $title;
    public $creationDatetime;
    public $updateDatetime;

    public static function getRepositoryClass(): string
    {
        return BookRepository::class;
    }
}