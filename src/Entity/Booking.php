<?php

namespace App\Entity;

use App\Repository\BookingRepository;

class Booking extends AbstractEntity
{
    public $id;
    public $customer;
    public $nbPlayer;
    public $date;
    public $creationDatetime;
    public $updateDatetime;

    public static function getRepositoryClass(): string
    {
        return BookingRepository::class;
    }
}