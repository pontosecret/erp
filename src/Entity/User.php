<?php

namespace App\Entity;

use DateTime;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class User extends AbstractEntity implements UserInterface
{
    #[ORM\Id]
    #[ORM\Column(type: 'string')]
    public ?string $id = null;
    
    #[ORM\Column(type: 'string')]
    public string $identifier = '';

    #[ORM\Column(type: 'text')]
    public string $jwt = '';
    
    #[ORM\Column(type: 'json')]
    public array $roles = [];

    #[ORM\Column(type: 'datetime', nullable: true)]
    public ?DateTime $lastLogin = null;

    public static function getRepositoryClass(): string
    {
        return '';
    }

    public function getUsername() 
    {
        return $this->getUserIdentifier();
    }

    public function getUserIdentifier()
    {
        return $this->identifier;
    }

    public function getRoles() 
    {
        return $this->roles;
    }

    public function getPassword()
    {
        return $this->jwt;
    }

    public function getSalt() 
    {
        return '';
    }

    public function eraseCredentials()
    {
        return;
    }
}