<?php

namespace App\Entity;

use JsonSerializable;
use ReflectionClass;

abstract class AbstractEntity implements JsonSerializable
{
    protected bool $cleanNullValues = false;
    protected ReflectionClass $reflection;

    public function __construct()
    {
        $this->cleanNullValues = true;
    }

    public static function hydrate($data)
    {
        $className = get_called_class();
        $instance = new $className();

        foreach ($data as $key => $value) {
            if (property_exists($instance, $key)) {
                $instance->__set($key, $value);
            }
        }

        return $instance;
    }

    abstract public static function getRepositoryClass(): string;

    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        }
    }

    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        }

        return $this;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function toArray() 
    {
        $data = [];
        foreach (get_object_vars($this) as $property => $value) {
            if ($this->reflect()->hasProperty($property) && $this->reflect()->getProperty($property)->isPublic()) {
                $data[$property] = $this->$property;
            }
        }

        if ($this->cleanNullValues) {
            $data = array_filter($data);
        }

        return $data;
    }

    protected function reflect() 
    {
        if( !isset( $this->reflection ) ) {
            $this->reflection = new ReflectionClass(get_called_class());
        }
        return $this->reflection;
    }
}