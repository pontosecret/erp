<?php

namespace App\Repository;

use App\Entity\Book;

class BookingRepository extends AbstractAPIRepository
{
    protected static $endpoint = '/bookings';
    protected static $entity = Booking::class;
}