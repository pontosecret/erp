<?php

namespace App\Repository;

use App\Entity\AbstractEntity;
use App\Service\APIConnector;

abstract class AbstractAPIRepository
{
    protected static $endpoint;
    protected static $entity;

    private APIConnector $apiConnector;

    public function __construct(APIConnector $apiConnector)
    {
        $this->apiConnector = $apiConnector;
    }

    public function find($id, $lockMode = null, $lockVersion = null)
    {
        $response   = $this->apiConnector->query('GET', get_called_class()::$endpoint.'/'.$id);
        $jsonData   = json_decode($response, true);

        return get_called_class()::$entity::hydrate($jsonData);
    }

    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
    {
        $response   = $this->apiConnector->query('GET', get_called_class()::$endpoint, $criteria);
        $jsonData   = json_decode($response, true);

        $objectList = [];
        foreach ($jsonData['hydra:member'] as $data) {
            $objectList[] = get_called_class()::$entity::hydrate($data);
        }

        return $objectList;
    }

    public function findOneBy(array $criteria, ?array $orderBy = null)
    {
        return $this->findBy($criteria, $orderBy, 1);
    }

    public function findAll()
    {
        return $this->findBy([]);
    }

    public function save(AbstractEntity $entity)
    {
        $entityData = $entity->toArray();

        if (array_key_exists('id', $entityData)) {
            $this->apiConnector->query('PUT', get_called_class()::$endpoint.'/'.$entityData['id'], $entityData);
        } else {
            $this->apiConnector->query('POST', get_called_class()::$endpoint, $entityData);
        }

    }
}