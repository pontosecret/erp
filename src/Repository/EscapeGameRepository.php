<?php

namespace App\Repository;

use App\Entity\EscapeGame;

class EscapeGameRepository extends AbstractAPIRepository
{
    protected static $endpoint = '/escape_games';
    protected static $entity = EscapeGame::class;
}