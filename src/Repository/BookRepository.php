<?php

namespace App\Repository;

use App\Entity\Book;

class BookRepository extends AbstractAPIRepository
{
    protected static $endpoint = '/books';
    protected static $entity = Book::class;
}