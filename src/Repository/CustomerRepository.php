<?php

namespace App\Repository;

use App\Entity\Customer;

class CustomerRepository extends AbstractAPIRepository
{
    protected static $endpoint = '/customers';
    protected static $entity = Customer::class;
}