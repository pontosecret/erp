<?php

namespace App\Controller;

use App\Entity\EscapeGame;
use App\Form\EscapeGameType;
use App\Repository\EscapeGameRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/escape-games")]
class EscapeGameController extends AbstractController
{
    #[Route("", name: "erp.escape-game.list")]
    public function list(EscapeGameRepository $escapeGameRepository) 
    {
        return $this->render('escape-game/list.html.twig', [
            'escapeGames' => $escapeGameRepository->findAll()
        ]);
    }

    #[Route("/create", name: "erp.escape-game.create")]
    public function create(Request $request, EscapeGameRepository $escapeGameRepository)
    {
        $escapeGame = new EscapeGame();
        $form = $this->createForm(EscapeGameType::class, $escapeGame);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $escapeGameRepository->save($form->getData());

            return $this->redirectToRoute('erp.escape-game.list');
        }

        return $this->render('escape-game/create.html.twig', [
            'form' => $form->createView() 
        ]);
    }

    #[Route("/update/{id}", name: "erp.escape-game.update")]
    public function update(string $id, Request $request, EscapeGameRepository $escapeGameRepository)
    {
        $escapeGame = $escapeGameRepository->find($id);
        $form = $this->createForm(EscapeGameType::class, $escapeGame);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $escapeGameRepository->save($form->getData());

            return $this->redirectToRoute('erp.escape-game.update', ['id' => $escapeGame->id]);
        }

        return $this->render('escape-game/update.html.twig', [
            'form' => $form->createView() 
        ]);
    }
}