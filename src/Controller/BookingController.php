<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Form\BookingType;
use App\Repository\BookingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/bookings")]
class BookingController extends AbstractController
{
    #[Route("", name: "erp.booking.list")]
    public function list(BookingRepository $bookingRepository) 
    {
        return $this->render('booking/list.html.twig', [
            'bookings' => $bookingRepository->findAll()
        ]);
    }

    #[Route("/create", name: "erp.booking.create")]
    public function create(Request $request, BookingRepository $bookingRepository)
    {
        $booking = new Booking();
        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bookingRepository->save($form->getData());

            return $this->redirectToRoute('erp.booking.list');
        }

        return $this->render('booking/create.html.twig', [
            'form' => $form->createView() 
        ]);
    }

    #[Route("/update/{id}", name: "erp.booking.update")]
    public function update(string $id, Request $request, BookingRepository $bookingRepository)
    {
        $booking = $bookingRepository->find($id);
        $form = $this->createForm(BookType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bookingRepository->save($form->getData());

            return $this->redirectToRoute('erp.booking.update');
        }

        return $this->render('booking/update.html.twig', [
            'form' => $form->createView() 
        ]);
    }
}