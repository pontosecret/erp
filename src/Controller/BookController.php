<?php

namespace App\Controller;

use App\Entity\Book;
use App\Form\BookType;
use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

#[Route("/books")]
class BookController extends AbstractController
{
    #[Route("", name: "erp.book.list")]
    public function list(BookRepository $bookRepository) 
    {
        return $this->render('book/list.html.twig', [
            'books' => $bookRepository->findAll()
        ]);
    }

    #[Route("/create", name: "erp.book.create")]
    public function create(Request $request, BookRepository $bookRepository)
    {
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bookRepository->save($form->getData());

            return $this->redirectToRoute('erp.book.create');
        }

        return $this->render('book/create.html.twig', [
            'form' => $form->createView() 
        ]);
    }

    #[Route("/update/{id}", name: "erp.book.update")]
    public function update(string $id, Request $request, BookRepository $bookRepository)
    {
        $book = $bookRepository->find($id);
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bookRepository->save($form->getData());

            return $this->redirectToRoute('erp.book.update', ['id' => $book->id]);
        }

        return $this->render('book/update.html.twig', [
            'form' => $form->createView() 
        ]);
    }

    #[Route("/book/search", name: "erp.book.search")]
    public function search(Request $request) 
    {
        $found = null;
        if ($request->request->count()) {
            $apiKey = "AIzaSyCOAFHTOuM0bv94JGYls8Y923405ax1Mtw";
            $isbn = $request->request->get('isbn');
            $name = $request->request->get('name');
            $parameters = [
                'q' => "",
                'key' => $apiKey
            ];
            if ($isbn) {
                $parameters['q'] = "isbn={$isbn}";
            }
            if ($name) {
                $parameters['q'] = $name;
            }
            $ch = curl_init("https://books.googleapis.com/books/v1/volumes?".http_build_query($parameters));
            curl_setopt_array($ch, [
                CURLOPT_HTTPHEADER => [
                    "Accept: application/json"
                ],
                CURLOPT_RETURNTRANSFER => true
            ]);

            $rawResponse = curl_exec($ch);

            $response = json_decode($rawResponse);

            $found = false;
            dump($response);
            if ($response->totalItems > 0 && isset($response->items)) {
                foreach($response->items as $book) {
                    foreach ($book->volumeInfo->industryIdentifiers as $identifier) {
                        if ($identifier->type === 'ISBN_13') {
                            if ($identifier->identifier === $isbn) {
                                $found = [
                                    'label' => $book->volumeInfo->title,
                                    'isbn'  => $identifier->identifier,
                                    'description' => $book->volumeInfo->description,
                                    'publishedDate' => $book->volumeInfo->publishedDate,
                                    'publisher' => $book->volumeInfo->publisher
                                ];
    
                                if (isset($book->volumeInfo->seriesInfo)) {
                                    $found['volume'] = $book->volumeInfo->seriesInfo->bookDisplayNumber;
                                    $found['serie'] = [
                                        
                                    ];
                                }
                                break 2;
                            }
                            break;
                        }
                    }
                }
            }

            dump($found);
        }

        return $this->render('/book/search.html.twig', [
            'book' => $found
        ]);
    }
}