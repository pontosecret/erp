<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    #[Route("/", name: "erp.homepage")]
    #[Route("/dashboard", name: "erp.dashboard")]
    public function index() 
    {
        return $this->render('dashboard.html.twig');
    }
}