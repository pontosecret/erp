<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    #[Route("/login", name: "erp.security.login", methods:["GET", "POST"])]
    public function login(Request $request)
    {
        return $this->render('/security/login.html.twig', [
            'lastUsername' => null,
            'error' => null
        ]);
    }

    #[Route("/logout", name: "erp.security.logout", methods:["GET"])]
    public function logout() {}
}