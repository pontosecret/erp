<?php

namespace App\Controller;

use App\Repository\CustomerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class CustomerController extends AbstractController
{
    #[Route("/customers", name: "erp.customer.list")]
    public function list(CustomerRepository $customerRepository) 
    {
        dump($customerRepository->findAll()); exit;
    }
}