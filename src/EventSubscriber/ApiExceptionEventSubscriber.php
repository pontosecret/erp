<?php

namespace App\EventSubscriber;

use App\Exception\ApiException;
use App\Exception\UnauthorizedApiException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ApiExceptionEventSubscriber implements EventSubscriberInterface
{   
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage){
        $this->tokenStorage = $tokenStorage;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => [
                ['processException', 0],
            ],
        ];
    }

    public function processException(ExceptionEvent $event) 
    {
        if ($event->getThrowable() instanceof ApiException) {
            switch ($event->getThrowable()::class) {
                case UnauthorizedApiException::class:
                    $this->tokenStorage->setToken(null);
                    $event->setResponse(new RedirectResponse('/login'));
                    break;
            }
        }
    }
}