<?php

namespace App\Exception;

class UnauthorizedApiException extends ApiException {}