<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;

class ApiFormAuthenticator extends AbstractLoginFormAuthenticator
{
    private SecurityManager $securityManager;
    private RouterInterface $router;

    public function __construct(RouterInterface $router, SecurityManager $securityManager)
    {
        $this->router          = $router;
        $this->securityManager = $securityManager;
    }

    public function getLoginUrl(Request $request): string
    {   
        return $this->router->generate('erp.security.login');
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return new RedirectResponse($this->router->generate('erp.dashboard'));
    }

    public function authenticate(Request $request): PassportInterface
    {
        $username = $request->request->get('username'); 
        $password = $request->request->get('password'); 

        return $this->securityManager->getPassport($username, $password);
    }
}
