<?php

namespace App\Security;

use App\Entity\User;
use App\Service\APIConnector;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Exception;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

class SecurityManager
{
    private APIConnector $apiConnector;
    private EntityManagerInterface $entityManager;

    public function __construct(APIConnector $apiConnector, EntityManagerInterface $entityManager)
    {
        $this->apiConnector = $apiConnector;
        $this->entityManager = $entityManager;
    }

    public function checkCredentials($credentials, $user)
    {
        if (!isset($credentials['token'])) {
            return false;
        }

        return true;
    }

    public function getPassport($username, $password) 
    {
        $response = $this->apiConnector->authenticate($username, $password);

        if ($response['token']) {
            $decoded = json_decode(base64_decode(explode(".", $response['token'])[1]), true);
            $user = $this->entityManager->getRepository(User::class)->find($decoded['username']) ?? new User();
            $user->id         = $decoded['username'];
            $user->identifier = $decoded['username'];
            $user->jwt        = $response['token'];
            $user->roles      = $decoded['roles'];
            $user->lastLogin  = new DateTime();

            $credentials = new CustomCredentials([$this, 'checkCredentials'], ['user' => $user, 'token' => $response['token']]);
            $passport    = new Passport(new UserBadge($username), $credentials);

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            return $passport;
        }

    }
}